"use strict"

let userName = prompt("What is your name?"); 
let defaultUserName = "";

// Якщо ім'я введено неправильно //

while (!userName) {
    alert("Enter the proper name please");

    // Якщо користувач натиснув "Отмена", в промпт 
    // виводиться дефолтне значення "Enter the proper name"

    if (userName === null) {
        defaultUserName = "Enter the proper name";
    }
    userName = prompt("What is your name?", defaultUserName); 
}

let userAge = prompt("What is your age?");
let defaultUserAge = "";

// Якщо в поле вік введено string або порожній рядок //

while (isNaN(userAge) || userAge == "") {
    alert("Enter the proper age please");

    // дефолтне значення віку є введена раніше інформація //

    defaultUserAge = userAge;         
    userAge = prompt("What is your age?", defaultUserAge);
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
}

else if (userAge <= 22) {
    let userContinue = confirm("Are you sure you want to continue?");
    if (userContinue == true) {
        alert(`Welcome, ${userName}!`);
    }
    else {
        alert("You are not allowed to visit this website");
    }
}

else {
    alert(`Welcome, ${userName}!`);
}